import React from "react";
import {
  Deck,
  Slide,
  FlexBox,
  Box,
  FullScreen,
  Progress,
  Heading,
  Link,
  Text,
  UnorderedList,
  ListItem,
  Notes,
  indentNormalizer,
  Image,
} from "spectacle";
import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-javascript';
import "ace-builds/src-noconflict/theme-monokai";

const theme = {
  colors: {
    primary: "#ffffff",
    secondary: "#ffffff",
    tertiary: "#000000",
  },
  fontSizes: {
    header: "64px",
    paragraph: "28px",
  },
};

function template({ slideNumber, numberOfSlides }) {
  return (
    <FlexBox
      justifyContent="space-between"
      position="absolute"
      bottom={0}
      width={1}
    >
      <Box padding="0 1em">
        <FullScreen />
      </Box>
      <Box padding="1em">
        <Progress />
      </Box>
    </FlexBox>
  );
}

class Frame extends React.Component {
  constructor(props) {
    super(props);
    this.frame = React.createRef();
  }
  render() {
      return <iframe title="code" ref={this.frame} style={{border: 'none', backgroundColor: 'white', width: '100%', height: '100%'}} />;
  }

  loadIt(props) {
    // first, we set the src to about:blank to clear out any running code, including timers
    // then, once that has loaded, load our intended code into the page
    this.frame.current.onload = () => {
      this.frame.current.contentDocument.open();
      this.frame.current.contentDocument.write(props.content);
      this.frame.current.contentDocument.close();
    };
    this.frame.current.src = "about:blank";
  }
  componentDidMount() {
    this.loadIt(this.props);
  }
  componentWillReceiveProps(nextProps) {
    this.loadIt(nextProps);
  }
}

class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: this.props.children,
      frame: this.props.children
    };
  }

  buildPage() {
    return `
      <!DOCTYPE html>
      <html>
        <head>
          <style>
            /* built-in styles for demo */
            * {
              font-size: 50px;
            }
            input[type="checkbox"] {
              height: 50px;
              width: 50px;
            }
            input[type="text"] {
              display: block;
              width: 100%;
            }
          </style>
          <script type="module">
            ${this.state.code}
          </script>
        </head>
        <body>

        </body>
      </html>`;
  }

  onChange(value, e) {
    this.setState({
      code: value
    });
  }

  render() {
    return <FlexBox height="70%" position="relative" onKeyUp={e => console.log.bind(console, e.key)}>
      <AceEditor
        mode="javascript"
        value={this.state.code}
        theme="monokai"
        fontSize={20}
        debounceChangePeriod={500}
        onChange={this.onChange.bind(this)}
        wrapEnabled
        width="100%"
        setOptions={{
          hasCssTransforms: true
        }}
      />
      <Frame content={this.buildPage()} />
    </FlexBox>
  }
}

function App() {
  return (
    <Deck theme={theme} template={template} transitionEffect="none">
      <Slide>
        <FlexBox>
          <Box>
            <Heading>David Alan Hjelle</Heading>
            <Text textAlign="center">
              <Link href="mailto:dahjelle@thehjellejar.com">
                dahjelle@thehjellejar.com
              </Link>
            </Text>
            <Text textAlign="center">
              Work: <Link href="https://iconcmo.com">https://iconcmo.com</Link>
            </Text>
          </Box>
          <Image src="./images/family.jpeg" maxWidth="40vw" overflow="hidden" width="50%" />
        </FlexBox>
      </Slide>
      <Slide>
        <Heading>Why React?</Heading>
        <FlexBox>
          <UnorderedList>
            <ListItem>components</ListItem>
            <ListItem>immediate mode GUI</ListItem>
            <ListItem>fast DOM</ListItem>
            <ListItem>XSS</ListItem>
            <ListItem>community</ListItem>
          </UnorderedList>
        </FlexBox>
        <Notes>
          Yes, I know this talk is about `lit-html`. Bear with me a moment.
        </Notes>
      </Slide>
      <Slide>
        <Heading>lit-html</Heading>
        <FlexBox>
          <UnorderedList>
            <ListItem>immediate mode GUI</ListItem>
            <ListItem>fast DOM</ListItem>
            <ListItem>XSS</ListItem>
            <ListItem>no components</ListItem>
          </UnorderedList>
          <UnorderedList>
            <ListItem>small</ListItem>
            <ListItem>no build step*</ListItem>
            <ListItem>
              <i>faster</i>
            </ListItem>
          </UnorderedList>
        </FlexBox>
        <Text textAlign="center">
          <Link href="https://lit-html.polymer-project.org/">
            https://lit-html.polymer-project.org/
          </Link>
        </Text>
        <Notes>{`
          - small = could maybe write it yourself, can certainly consume code
          - react is 35 KB, lit-html maybe 5 KB?
          - components could be done with web components
          - pretty simple to include in an existing project
        `}</Notes>
      </Slide>
      <Slide>
        <Heading>Faster</Heading>
        <FlexBox>
        <table>
          <thead>
            <tr>
              <th>
              </th>
              <th>lit-html</th>
              <th>React</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>
                <div>creating 1,000 rows</div>
              </th>
              <td>
                <span>130.2</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>181.7</span>
                <br />
                <span>(1.40)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>updating all 1,000 rows (5 warmup runs)</div>
              </th>
              <td>
                <span>124.4</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>147.0</span>
                <br />
                <span>(1.18)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>
                  updating every 10th row for 1,000 rows (3 warmup runs)
                </div>
              </th>
              <td>
                <span>165.2</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>218.5</span>
                <br />
                <span>(1.32)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>
                  highlighting a selected row (no warmup runs)
                </div>
              </th>
              <td>
                <span>90.5</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>124.7</span>
                <br />
                <span>(1.38)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>
                  swap 2 rows for table with 1,000 rows (5 warmup runs)
                </div>
              </th>
              <td>
                <span>50.7</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>455.4</span>
                <br />
                <span>(8.98)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>removing one row (5 warmup runs)</div>
              </th>
              <td>
                <span>22.7</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>24.1</span>
                <br />
                <span>(1.06)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>creating 10,000 rows</div>
              </th>
              <td>
                <span>1,161.6</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>1,823.0</span>
                <br />
                <span>(1.57)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>
                  appending 1,000 to a table of 10,000 rows
                </div>
              </th>
              <td>
                <span>261.2</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>337.6</span>
                <br />
                <span>(1.29)</span>
              </td>
            </tr>
            <tr>
              <th>
                <div>clearing a table with 1,000 rows</div>
              </th>
              <td>
                <span>130.4</span>
                <br />
                <span>(1.00)</span>
              </td>
              <td>
                <span>148.4</span>
                <br />
                <span>(1.14)</span>
              </td>
            </tr>
          </tbody>
        </table>
        </FlexBox>
        <Text textAlign="center">Source: <Link href="https://krausest.github.io/js-framework-benchmark/2020/table_chrome_86.0.4240.75.html">JS Framework Benchmark</Link></Text>
      </Slide>
      <Slide>
        <Heading>Basic Rendering</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            render(html\`<h1>Hello!</h1>\`, document.body);
          `)}
        </Editor>
      </Slide>
      <Slide>
        <Heading>Dynamic Rendering</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            let counter = 0;

            function show() {
              let greeting = "World";
              if (counter % 2 === 0) {
                greeting = "Everyone";
              }
              render(html\`<h1>Hello, \${greeting}!</h1>\`, document.body);
              counter += 1;
            }

            show();
            setInterval(show, 1000);
          `)}
        </Editor>
      </Slide>
      <Slide>
        <Heading>Attributes</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            const type = "checkbox";
            render(html\`<label><input type=\${type} />Check me!</label>\`, document.body);
          `)}
        </Editor>
      </Slide>
      <Slide>
        <Heading>Boolean Attributes</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            const disabled = true;
            render(html\`
              <input type="text" ?disabled=\${disabled} value="one" />
              <input type="text" ?disabled=\${!disabled} value="two" />
            \`, document.body);
          `)}
        </Editor>
        <Notes>Present if true, not present if not.</Notes>
      </Slide>
      {/* <Slide>
        <Heading>Properties</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            function show(value) {
              render(html\`<input .value=\${value} />\`, document.body);
            }

            let counter = 0;

            show(counter);

            setInterval(() => {
              show(counter);
              counter += 1;
            }, 1000);
          `)}
        </Editor>
        <Notes>Useful mostly for form elements and web components. Properties are generally the `.js` version of an attribute, while the attribute is the original value.</Notes>
      </Slide> */}
      <Slide>
        <Heading>Event Listeners</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            function onClick(e) {
              window.alert("Go!");
            }

            render(html\`<button @click=\${onClick}>Go!</button>\`, document.body);
          `)}
        </Editor>
      </Slide>
      <Slide>
        <Heading>Nesting</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            const one = html\`<li>One</li>\`;
            const two = html\`<li>Two</li>\`;
            render(html\`<ul>\${one}\${two}</ul>\`, document.body);
          `)}
        </Editor>
      </Slide>
      <Slide>
        <Heading>Arrays</Heading>
        <Editor>
          {indentNormalizer(`
            import {html, render} from 'https://unpkg.com/lit-html?module';

            const items = [];
            items.push(html\`<li>One</li>\`);
            items.push(html\`<li>Two</li>\`);
            render(html\`<ul>\${items}</ul>\`, document.body);
          `)}
        </Editor>
      </Slide>
      <Slide>
        <Heading><i>Not</i> Tag Names!</Heading>
      </Slide>
      <Slide>
        <Heading>Directives</Heading>
        <FlexBox>
          <Box>
            <Text>customize how rendering happens</Text>
            <UnorderedList>
              <ListItem><Link href="https://lit-html.polymer-project.org/guide/template-reference#classmap">classMap</Link></ListItem>
              <ListItem><Link href="https://lit-html.polymer-project.org/guide/template-reference#stylemap">styleMap</Link></ListItem>
              <ListItem><Link href="https://lit-html.polymer-project.org/guide/template-reference#repeat">repeat</Link></ListItem>
              <ListItem>several more, or <Link href="https://lit-html.polymer-project.org/guide/creating-directives">create your own</Link></ListItem>
            </UnorderedList>
          </Box>
        </FlexBox>
      </Slide>
      <Slide>
        <Heading>How?</Heading>
        <FlexBox>
          <Box>
            <UnorderedList>
              <ListItem>tagged template literal</ListItem>
              <ListItem>HTML template tag</ListItem>
              <ListItem>lots of caching</ListItem>
            </UnorderedList>
            <Text>See more at <Link href="https://lit-html.polymer-project.org/guide/concepts">the lit-html site</Link>.</Text>
          </Box>
        </FlexBox>
      </Slide>
      <Slide>
        <Heading>Questions?</Heading>
      </Slide>
    </Deck>
  );
}
export default App;

// not sure if this is necessary, but I seemed to have run into it once while testing, so why now?
document.onkeydown = function( event ) { // document object seems to be sufficient to catch backspace; other keys may need to listen on the window
  // this function is meant to be called on a keypress handler and filter out backspace presses that aren't in text fields
  // this is used to stop some Windows browsers from using backspace as a shortcut for the "back" action
  /****************************************************************
  * REFERENCES:   - http://www.nollegcraft.com/code/GoogleChromeExtension/BackspaceMeansBackspace/index.html
  *               - http://stackoverflow.com/questions/1495219/how-can-i-prevent-the-backspace-key-from-navigating-back
  *               - http://forums.mozillazine.org/viewtopic.php?f=25&t=432726
  *               - http://www.sitepoint.com/forums/showthread.php?168890-disable-back-for-javascript
  *****************************************************************/
  if ( event.key === 'Backspace' )  {
    const target = event.target;
    const tag = target.tagName.toLowerCase();
    const type = target.type && target.type.toLowerCase();
    // only stop if...
    // TODO: break this up a bit to make clearer
    if ((tag !== "input" /* we aren't an input field */ || (tag === "input"&& (/* or, if it is an input field, we only care about text and password fields */ type !== "text"&& type !== "password" && type !== "search") ) ) && tag !== "textarea" /* we aren't a textarea field */ ) {
      event.preventDefault();
      event.stopPropagation();
    }
  }
};